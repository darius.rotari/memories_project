Numele proiectului : Memories

Scurta descriere a proiectului:

Memories este un proiect pe care l-am conceput, bazandu-ma pe ceea ce ofera atat Facebook, cat si Pinterest.Este o platforma pe care utilizatorii pot incarca amintiri, dorindu-si sa le impartaseasca si cu alti utilizatori.
Aceste amintiri pot fi reprezentate de un loc care ti-a placut sau care nu ti-a placut, sau poate de o experienta culturala care te-a incantat, sau de o surpriza pe care cei apropiati ti-au facut-o.Amintirile postate pot fi dintre cele mai diverse, principalul rol al site-ului creat este de a impartasi experiente, fie ele bune sau mai putin bune. 
Un simplu utilizator, care nu are cont facut pe Memories, poate doar sa vizualizeze postarile celorlalti utilizatori.
In momentul in care un utilizator isi face cont(este necesara completarea unui formular in care i se cer Numele, Prenumele, Adresa de e-mail si parola), el poate crea o postare noua(in care va aparea titlul, un mesaj ce poate fi o scurta descriere, numeroase tag-uri, precum si o poza). La apasarea butonului "SUBMIT", postarea sa va fi vizibila de catre ceilalti utilizatori. 
Un alt lucru pe care utilizatorul il poate face este sa dea like postarilor, prin apasarea butonului "Like" ce se afla sub fiecare postare. De asemenea, el poate edita o postare pe care a facut-o anterior(prin apasarea punctelor din dreapta sus aferente postarii) sau o poate sterge(prin apasarea butonului "DELETE").

Lansarea in executie a aplicatiei se face folosind comanda "npm start"

Tehnologiile folosite:

REACT : Am utilizat React pentru a dezvolta interfața utilizatorului (UI) a aplicației mele. Am creat componente reutilizabile pentru a organiza și afișa conținutul aplicației.

JAVASCRIPT :  A fost utilizat pentru dezvoltarea proiectului, asigurând funcționalități interactive și dinamice.

CSS : Am folosit si un fisier CSS pentru gestionarea stilizării și aspectului aplicației(pentru culoarea de fundal si fontul titlului).